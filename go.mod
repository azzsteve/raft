module gitlab.com/azzsteve/raft

go 1.15

require (
	github.com/fortytw2/leaktest v1.3.0
	github.com/go-logr/logr v0.2.0
	github.com/go-logr/zapr v0.2.0
	github.com/google/uuid v1.1.2
	go.uber.org/zap v1.16.0
)
