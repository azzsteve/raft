# Raft

Implementation of the [Raft consensus algorithim](https://raft.github.io).

## Code

All the code follows the blog series [Implementing
Raft](https://eli.thegreenplace.net/2020/implementing-raft-part-0-introduction/)
by [Eli Bendersky](https://eli.thegreenplace.net/pages/about).
