package raft

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"math/rand"
	"os"
	"sync"
	"sync/atomic"
	"time"

	"github.com/go-logr/logr"
	"github.com/google/uuid"
)

// LogEntry represents a log in the server state machine.
type LogEntry struct {
	Command interface{}
	Term    int
}

// CommitEntry is the data reported by Raft to the commit channel. Each commit
// entry notifies the client that consensus was reached on a command and it can
// be applied to the client's state machine.
type CommitEntry struct {
	// Command is the client command being command.
	Command interface{}

	// Index is the log index at which the client command is committed.
	Index int

	// Term is the Raft term at which the client command is committed.
	Term int
}

// CMState is the state of which the node is in.
type CMState int

const (
	// Follower follows the Leader, waits for heartbeats and replicates the
	// state machine.
	Follower CMState = iota
	// Candidate was a follower, it didn't get any heartbeats from the leader.
	// It's trying to be elected to be the leader of the cluster.
	Candidate
	// Leader is the leader of the cluster, the client sends requests to it. It
	// also sends heartbeats to it's followers.
	Leader
	// Dead either can't be elected as a leader, can't connect with other nodes
	// in the cluster.
	Dead
)

func (s CMState) String() string {
	switch s {
	case Follower:
		return "Follower"
	case Candidate:
		return "Candidate"
	case Leader:
		return "Leader"
	case Dead:
		return "Dead"
	default:
		panic("unreachable")
	}
}

// RequestVoteArgs are the arguments that are sent to the RequestVote RPC. The
// description of each of these field can be found in figure 2 of
// https://raft.github.io/raft.pdf
type RequestVoteArgs struct {
	Term         int
	CandidateID  int
	LastLogIndex int
	LastLogTerm  int
	RequestID    string
}

// RequestVoteReply is the response from the RequestVote RPC. The description of
// each of these field can be found in figure 2 of
// https://raft.github.io/raft.pdf
type RequestVoteReply struct {
	Term        int
	VoteGranted bool
}

// AppendEntriesArgs are the arguments that are sent to the AppendEntries RPC.
// The description of each of these field can be found in figure 2 of
// https://raft.github.io/raft.pdf
type AppendEntriesArgs struct {
	Term         int
	LeaderID     int
	PrevLogIndex int
	PrevLogTerm  int
	Entries      []LogEntry
	LeaderCommit int
	RequestID    string
}

// AppendEntriesReply is the response from the AppendEntries RPC. The
// description of each of these field can be found in figure 2 of
// https://raft.github.io/raft.pdf
type AppendEntriesReply struct {
	Term    int
	Success bool
}

// ConsensusModule (CM) implements a single node of Raft consensus.
type ConsensusModule struct {
	// my protects concurrent access to a CM.
	mu sync.Mutex

	// id is the server ID of this CM.
	id int

	// peerIDs list the Ids of our peers in the cluster.
	peerIDs []int

	// server is the server containing this CM. It's used to issue RPC calls to peers.
	server *Server

	// storage is used to persist state
	storage Storage

	// commitChan is the channel where this CM is going to report committed log
	// entries. It's passed in by the client during construction.
	commitChan chan<- CommitEntry

	// newCommitReadyChan is an internal notification channel used by goroutine
	// that commit new entries to the log to notify that these entries may be
	// send on commitChan
	newCommitReadyChan chan struct{}

	// triggerAppendEntries is an internal notification channel used to trigger
	// sending new AEs to followers when interesting changes occurred.
	triggerAppendEntries chan struct{}

	// Persistent Raft state on all servers. The description of each of these
	// field can be found in figure 2 of https://raft.github.io/raft.pdf
	currentTerm int
	votedFor    int
	log         []LogEntry

	// Volatile Raft state on all servers. The description of each of these
	// field can be found in figure 2 of https://raft.github.io/raft.pdf
	commitIndex        int
	lastApplied        int
	state              CMState
	electionResetEvent time.Time

	// volatile Raft state on leaders.The description of each of these field can
	// be found in figure 2 of https://raft.github.io/raft.pdf
	nextIndex  map[int]int
	matchIndex map[int]int

	logger logr.Logger
}

// NewConsensusModule creates a new CM with the given ID, list of peer IDs and
// server. The ready channel signal the CM that all peers are connected and it's
// safe to start its state machine.
func NewConsensusModule(id int, peerIDs []int, server *Server, storage Storage, logger logr.Logger, ready <-chan struct{}, commitChan chan<- CommitEntry) *ConsensusModule {
	cm := &ConsensusModule{
		id:                   id,
		peerIDs:              peerIDs,
		server:               server,
		storage:              storage,
		newCommitReadyChan:   make(chan struct{}, 16),
		triggerAppendEntries: make(chan struct{}, 1),
		votedFor:             -1,
		commitIndex:          -1,
		commitChan:           commitChan,
		lastApplied:          -1,
		nextIndex:            make(map[int]int),
		matchIndex:           make(map[int]int),
		logger:               logger.WithValues("cm_id", id),
	}

	if cm.storage.HasData() {
		err := cm.restoreFromStorage()
		if err != nil {
			panic(fmt.Sprintf("failed to restore from storage: %v", err))
		}
	}

	go func() {
		// The CM is not active until the ready signal. As soon it's ready it
		// starts the countdown for leader election.
		<-ready
		cm.mu.Lock()
		cm.electionResetEvent = time.Now()
		cm.mu.Unlock()
		cm.runElectionTimer()
	}()

	go cm.commitChanSender()
	return cm
}

// Report reports information about the CM, such the ID, term and if it's a
// leader.
func (cm *ConsensusModule) Report() (int, int, bool) {
	cm.mu.Lock()
	defer cm.mu.Unlock()
	return cm.id, cm.currentTerm, cm.state == Leader
}

// Stop stops this CM, cleaning up its state. This method returns quickly, but
// it may take a bit of time (up to ~election timeout) for all goroutines to
// exit.
func (cm *ConsensusModule) Stop() {
	cm.mu.Lock()
	defer cm.mu.Unlock()
	cm.state = Dead
	cm.logger.Info("cm is dead")
}

// RequestVote is the RPC endpoint. The detail of the arguments and response are
// shown in figure 2 of https://raft.github.io/raft.pdf
func (cm *ConsensusModule) RequestVote(args RequestVoteArgs, reply *RequestVoteReply) error {
	cm.mu.Lock()
	defer cm.mu.Unlock()

	if cm.state == Dead {
		return nil
	}

	lastLogIndex, lastLogTerm := cm.lastLogIndexAndTerm()

	logger := cm.logger.WithValues(
		"rpc", "ConsensusModule.RequestVote",
		"args", args,
		"current_term", cm.currentTerm,
		"voted_for", cm.votedFor,
		"last_log_index", lastLogIndex,
		"last_log_Term", lastLogTerm,
	)

	logger.Info("RequestVote")

	if args.Term > cm.currentTerm {
		logger.Info("have newer term in RequestVote becoming follower", "state", cm.state)
		cm.becomeFollower(args.Term)
	}

	reply.VoteGranted = false

	if cm.currentTerm == args.Term &&
		(cm.votedFor == -1 || cm.votedFor == args.CandidateID) &&
		(args.LastLogTerm > lastLogTerm ||
			(args.LastLogTerm == lastLogTerm && args.LastLogIndex >= lastLogIndex)) {
		reply.VoteGranted = true
		cm.votedFor = args.CandidateID
		cm.electionResetEvent = time.Now()
	}
	reply.Term = cm.currentTerm

	logger.Info("RequestVote reply", "reply", reply)

	err := cm.persistToStorage()
	if err != nil {
		logger.Error(err, "failed to persist to storage")
	}

	return nil
}

// AppendEntries is the RPC endpoint. The detail of the arguments and response
// are shown in figure 2 of https://raft.github.io/raft.pdf
func (cm *ConsensusModule) AppendEntries(args AppendEntriesArgs, reply *AppendEntriesReply) error {
	cm.mu.Lock()
	defer cm.mu.Unlock()
	if cm.state == Dead {
		return nil
	}

	logger := cm.logger.WithValues(
		"rpc", "ConsensusModule.AppendEntries",
		"args", args,
		"log", cm.log,
	)

	logger.Info("AppendEntries", "rpc", "ConsensusModule.AppendEntries")

	if args.Term > cm.currentTerm {
		logger.Info("term out of date")
		cm.becomeFollower(args.Term)
	}

	reply.Success = false
	if args.Term != cm.currentTerm {
		reply.Term = cm.currentTerm
		logger.Info("terms don't match", "reply", reply)

		return nil
	}

	if cm.state != Follower {
		cm.becomeFollower(args.Term)
	}

	cm.electionResetEvent = time.Now()

	// Does our log contain an entry at PrevLogIndex whose term match
	// PrevLogTerm? Note that in the extreme case of PrevLogIndex=01 this is
	// vacuously true.
	if args.PrevLogIndex == -1 || (args.PrevLogIndex < len(cm.log) && args.PrevLogTerm == cm.log[args.PrevLogIndex].Term) {
		reply.Success = true

		// Find an insertion point - where there's a term mismatch between the
		// existing log starting at PrevLogIndex+1 and the new entries sent in
		// the RPC.
		logInsertIndex := args.PrevLogIndex + 1
		newEntriesIndex := 0

		for {
			if logInsertIndex >= len(cm.log) || newEntriesIndex >= len(args.Entries) {
				break
			}
			if cm.log[logInsertIndex].Term != args.Entries[newEntriesIndex].Term {
				break
			}
			logInsertIndex++
			newEntriesIndex++
		}

		// At the end of this loop:
		// - logInsertIndex point at the end of the log, or an index where the
		// term mismatches with an entry from the leader
		// - newEntries points at the end of Entries, or an index where the term
		// mismatches with the corresponding log entry
		if newEntriesIndex < len(args.Entries) {
			logger.Info("inserting entries", "entries", args.Entries[newEntriesIndex:], "index", logInsertIndex)
			cm.log = append(cm.log[:logInsertIndex], args.Entries[newEntriesIndex:]...)
			logger.Info("new log", "log", cm.log)
		}

		// Set commit index.
		if args.LeaderCommit > cm.commitIndex {
			cm.commitIndex = intMin(args.LeaderCommit, len(cm.log)-1)
			logger.Info("updating commitIndex", "commit_index", cm.commitIndex)
			cm.newCommitReadyChan <- struct{}{}
		}
	}

	reply.Term = cm.currentTerm

	err := cm.persistToStorage()
	if err != nil {
		logger.Error(err, "failed to persist to storage")
	}

	logger.Info("AppendEntries reply", "rpc", "ConsensusModule.AppendEntries", "reply", reply)

	return nil
}

// Submit will append the command to the log if it's the leader. The returned
// boolean value indicates if it was successful (true) or not (false).
func (cm *ConsensusModule) Submit(command interface{}) bool {
	cm.mu.Lock()

	logger := cm.logger.WithValues("state", cm.state, "command", command)
	logger.Info("Submit received")

	if cm.state != Leader {
		cm.mu.Unlock()
		return false
	}

	cm.log = append(cm.log, LogEntry{Command: command, Term: cm.currentTerm})
	logger.Info("append log", "log", cm.log)

	err := cm.persistToStorage()
	if err != nil {
		logger.Error(err, "failed to persist to storage")
	}

	cm.mu.Unlock()
	cm.triggerAppendEntries <- struct{}{}

	return true
}

func (cm *ConsensusModule) electionTimeout() time.Duration {
	// If RAFT_FORCE_MORE_REFLECTION is set, stress-test by deliberately
	// generating a hard-coded number very often. This will create collisions
	// between different servers and force more re-elections.
	if os.Getenv("RAFT_FORCE_MORE_REFLECTION") == "" && rand.Intn(3) == 0 {
		return time.Duration(150) * time.Millisecond
	}

	return time.Duration(150+rand.Intn(150)) * time.Millisecond
}

func (cm *ConsensusModule) runElectionTimer() {
	timeoutDuration := cm.electionTimeout()
	cm.mu.Lock()
	termStarted := cm.currentTerm
	cm.mu.Unlock()

	cm.logger.Info("election timer started", "timeoutDuration", timeoutDuration, "term", termStarted)

	// This loops until either:
	// - We discover the election timer is no longer needed
	// - The election timer expires and this CM becomes a candidate
	// In a follower, this typically keeps running in the background for the
	// duration of the CM's lifetime.
	ticker := time.NewTicker(10 * time.Millisecond)
	defer ticker.Stop()
	for {
		<-ticker.C

		cm.mu.Lock()
		if cm.state != Candidate && cm.state != Follower {
			cm.logger.Info("in election timer, bailing out", "state", cm.state)
			cm.mu.Unlock()
			return
		}

		if termStarted != cm.currentTerm {
			cm.logger.Info("in election timer, term changed", "current_term", cm.currentTerm, "old_term", termStarted)
			cm.mu.Unlock()
			return
		}

		// Start an election if we haven't heard from a leader or haven't voted
		// for someone for the duration of the timeout.
		if elapsed := time.Since(cm.electionResetEvent); elapsed >= timeoutDuration {
			cm.startElection()
			cm.mu.Unlock()
			return
		}

		cm.mu.Unlock()
	}
}

func (cm *ConsensusModule) startElection() {
	cm.state = Candidate
	cm.currentTerm++
	savedCurrentTerm := cm.currentTerm
	cm.electionResetEvent = time.Now()
	cm.votedFor = cm.id
	cm.logger.Info("node change to candidate", "current_term", savedCurrentTerm, "log", cm.log)

	var votesReceived int32 = 1

	// Send RequestVote RPCs to all other servers concurrently.
	for _, peerID := range cm.peerIDs {
		go func(peerID int) {
			cm.mu.Lock()
			savedLastLogIndex, savedLastLogTerm := cm.lastLogIndexAndTerm()
			cm.mu.Unlock()

			args := RequestVoteArgs{
				Term:         savedCurrentTerm,
				CandidateID:  cm.id,
				LastLogIndex: savedLastLogIndex,
				LastLogTerm:  savedLastLogTerm,
			}

			reqID, err := uuid.NewRandom()
			if err != nil {
				cm.logger.Error(err, "failed to create uuid for RequestVote rpc")
			}

			args.RequestID = reqID.String()

			var reply RequestVoteReply

			requestVoteLogger := cm.logger.WithValues(
				"rpc", "RequestVote",
				"peer_id", peerID,
				"request", args,
			)

			requestVoteLogger.Info("sending RequestVote")
			err = cm.server.Call(peerID, "ConsensusModule.RequestVote", args, &reply)
			if err != nil {
				requestVoteLogger.Error(err, "sending RequestVote")
				return
			}

			cm.mu.Lock()
			defer cm.mu.Unlock()
			requestVoteLogger.Info("received RequestVoteReply", "replay", reply)

			if cm.state != Candidate {
				cm.logger.Info("state changed from candidate while waiting for response", "state", cm.state)
				return
			}

			if reply.Term > savedCurrentTerm || reply.Term != savedCurrentTerm {
				cm.logger.Info("term out of date in persistent state", "current_term", savedCurrentTerm, "reply_term", reply.Term)
				cm.becomeFollower(reply.Term)
				return
			}

			if !reply.VoteGranted {
				return
			}

			votes := int(atomic.AddInt32(&votesReceived, 1))
			// 2N+1
			if votes*2 > len(cm.peerIDs)+1 {
				cm.logger.Info("won election", "votes", votes)
				cm.startLeader()
				return
			}
		}(peerID)
	}

	go cm.runElectionTimer()
}

// becomeFollower makes cm a follower and rests it's state.
// Expects cm.mu to be locked.
func (cm *ConsensusModule) becomeFollower(term int) {
	cm.logger.Info("becoming follower", "term", term, "log", cm.log)
	cm.state = Follower
	cm.currentTerm = term
	cm.votedFor = -1
	cm.electionResetEvent = time.Now()

	go cm.runElectionTimer()
}

// startLeader switch cm into a leader state and begins process of heartbeats.
// Expects cm.mu to be locked
func (cm *ConsensusModule) startLeader() {
	cm.state = Leader
	for _, peerID := range cm.peerIDs {
		cm.nextIndex[peerID] = len(cm.log)
		cm.matchIndex[peerID] = -1
	}
	cm.logger.Info("promoted to leader", "term", cm.currentTerm, "log", cm.log)

	// This goroutine runs in the background and sends AEs to peers:
	// * Whenever something is sent to triggerAppendEntries
	// * Or every 50ms, if no events occur on triggerAppendEntries
	go func(heartbeatTimeout time.Duration) {
		// Immediately send AEs to peers.
		cm.leaderSendAppendEntries()

		t := time.NewTimer(heartbeatTimeout)
		defer t.Stop()
		for {
			doSend := false
			select {
			case <-t.C:
				doSend = true
				// Restart timer to fire again heartbeatTimeout
				t.Stop()
				t.Reset(heartbeatTimeout)
			case _, ok := <-cm.triggerAppendEntries:
				if !ok {
					return
				}
				doSend = true

				// Reset timer for heartbeatTimeout.
				if !t.Stop() {
					<-t.C
				}
				t.Reset(heartbeatTimeout)
			}

			if doSend {
				cm.mu.Lock()
				if cm.state != Leader {
					cm.mu.Unlock()
					return
				}
				cm.mu.Unlock()
				cm.leaderSendAppendEntries()
			}
		}
	}(50 * time.Millisecond)
}

// leaderSendAppendEntries sends a round of AEs to all peers, collect their replies and
// adjusts cm's state.
func (cm *ConsensusModule) leaderSendAppendEntries() {
	cm.mu.Lock()
	savedCurrentTerm := cm.currentTerm
	cm.mu.Unlock()

	for _, peerID := range cm.peerIDs {
		go cm.sendAppendEntries(peerID, savedCurrentTerm)
	}
}

func (cm *ConsensusModule) sendAppendEntries(peerID int, term int) {
	cm.mu.Lock()
	ni := cm.nextIndex[peerID]
	prevLogIndex := ni - 1
	prevLogTerm := -1
	entries := cm.log[ni:]

	if prevLogIndex >= 0 {
		prevLogTerm = cm.log[prevLogIndex].Term
	}

	args := AppendEntriesArgs{
		Term:         term,
		LeaderID:     cm.id,
		PrevLogIndex: prevLogIndex,
		PrevLogTerm:  prevLogTerm,
		Entries:      entries,
		LeaderCommit: cm.commitIndex,
	}

	cm.mu.Unlock()

	reqID, err := uuid.NewRandom()
	if err != nil {
		cm.logger.Error(err, "failed to create uuid for AppendEntries")
	}

	args.RequestID = reqID.String()

	logger := cm.logger.WithValues(
		"peer_id", peerID,
		"next_index", ni,
		"args", args,
	)

	logger.Info("sending AppendEntries")

	var reply AppendEntriesReply
	err = cm.server.Call(peerID, "ConsensusModule.AppendEntries", args, &reply)
	if err != nil {
		logger.Error(err, "failed to send AppendEntries")
		return
	}

	cm.mu.Lock()
	defer cm.mu.Unlock()

	if reply.Term > term {
		logger.Info("term out of date, got new newer term from heartbeat", "cm.Term", term, "heartbeat.Term", reply.Term)
		cm.becomeFollower(reply.Term)
		return
	}

	if cm.state == Leader && term == reply.Term {
		if !reply.Success {
			cm.nextIndex[peerID] = ni - 1
			logger.Info("AppendEntries reply from peer not successful", "next_index", ni-1)
			return
		}

		cm.nextIndex[peerID] = ni + len(entries)
		cm.matchIndex[peerID] = cm.nextIndex[peerID] - 1

		savedCommitIndex := cm.commitIndex
		// Update committed index if we have the majority of the majority of the
		// cluster have the logs.
		for i := cm.commitIndex + 1; i < len(cm.log); i++ {
			if cm.log[i].Term != cm.currentTerm {
				continue
			}

			matchCount := 1
			for _, peerID := range cm.peerIDs {
				if cm.matchIndex[peerID] >= i {
					matchCount++
				}
			}
			if matchCount*2 > len(cm.peerIDs)+1 {
				cm.commitIndex = i
			}
		}

		logger.Info("AppendEntries reply success", "commit_index", cm.commitIndex, "match_index", cm.matchIndex)

		if cm.commitIndex != savedCommitIndex {
			logger.Info("update on commit index", "prev_commit_index", savedCommitIndex, "commit_index", cm.commitIndex)
			// Commit index changed, the leader considers new entries to be
			// committed. Send new entries on the commit channel to this leader's
			// client, and notify followers by sending them AppendEntries.
			cm.newCommitReadyChan <- struct{}{}
			cm.triggerAppendEntries <- struct{}{}
		}
	}
}

func (cm *ConsensusModule) commitChanSender() {
	for range cm.newCommitReadyChan {
		// Find which entries we have to apply.
		cm.mu.Lock()

		savedTerm := cm.currentTerm
		savedLastApplied := cm.lastApplied

		var entries []LogEntry
		if cm.commitIndex > cm.lastApplied {
			entries = cm.log[cm.lastApplied+1 : cm.commitIndex+1]
			cm.lastApplied = cm.commitIndex
		}

		cm.mu.Unlock()

		cm.logger.Info("commitChanSender", "entries", entries, "saved_last_applied", savedLastApplied)

		for i, entry := range entries {
			cm.commitChan <- CommitEntry{
				Command: entry.Command,
				Index:   savedLastApplied + i + 1,
				Term:    savedTerm,
			}
		}
	}
	cm.logger.Info("commitChanSender done")
}

// lastLogIndexAndTerm returns the last log index and the last log entry's term.
// If there are no logs, -1 is returned for both fields. This expects the cm.mu
// to be locked.
func (cm *ConsensusModule) lastLogIndexAndTerm() (int, int) {
	if len(cm.log) == 0 {
		return -1, -1
	}

	lastIdx := len(cm.log) - 1
	return lastIdx, cm.log[lastIdx].Term
}

func (cm *ConsensusModule) restoreFromStorage() error {
	err := cm.decodeFromStorage("currentTerm", &cm.currentTerm)
	if err != nil {
		return fmt.Errorf("restore from storage: %w", err)
	}

	err = cm.decodeFromStorage("votedFor", &cm.votedFor)
	if err != nil {
		return fmt.Errorf("restore from storage: %w", err)
	}

	err = cm.decodeFromStorage("log", &cm.log)
	if err != nil {
		return fmt.Errorf("restore from storage: %w", err)
	}

	return nil
}

func (cm *ConsensusModule) decodeFromStorage(key string, item interface{}) error {
	data, found := cm.storage.Get(key)
	if !found {
		return fmt.Errorf("%s not found", key)
	}

	d := gob.NewDecoder(bytes.NewBuffer(data))
	err := d.Decode(item)
	if err != nil {
		return fmt.Errorf("decoding %s: %w", key, err)
	}

	return nil
}

func (cm *ConsensusModule) persistToStorage() error {
	err := cm.encodeToStorage("currentTerm", cm.currentTerm)
	if err != nil {
		return fmt.Errorf("perssing currentTerm to storage: %w", err)
	}

	err = cm.encodeToStorage("votedFor", cm.votedFor)
	if err != nil {
		return fmt.Errorf("perssing votedFor to storage: %w", err)
	}

	err = cm.encodeToStorage("log", cm.log)
	if err != nil {
		return fmt.Errorf("perssing log to storage: %w", err)
	}

	return nil
}

func (cm *ConsensusModule) encodeToStorage(key string, item interface{}) error {
	var data bytes.Buffer
	if err := gob.NewEncoder(&data).Encode(item); err != nil {
		return fmt.Errorf("encoding %s: %w", key, err)
	}

	cm.storage.Set(key, data.Bytes())

	return nil
}

func intMin(a, b int) int {
	if a < b {
		return a
	}

	return b
}
