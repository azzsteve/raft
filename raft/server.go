package raft

import (
	"fmt"
	"math/rand"
	"net"
	"net/rpc"
	"os"
	"sync"
	"time"

	"github.com/go-logr/logr"
)

// Server is used to connect to other raft servers in the cluster to send RPC
// calls.
type Server struct {
	mu sync.Mutex

	cm       *ConsensusModule
	storage  Storage
	rpcProxy *RPCProxy

	rpcServer *rpc.Server
	listener  net.Listener

	id      int
	peerIDs []int

	commitChan  chan<- CommitEntry
	peerClients map[int]*rpc.Client

	ready <-chan struct{}
	quit  chan struct{}
	wg    sync.WaitGroup

	logger logr.Logger
}

// NewServer creates
func NewServer(id int, peerIDs []int, storage Storage, logger logr.Logger, ready <-chan struct{}, commitChan chan<- CommitEntry) *Server {
	s := &Server{
		id:          id,
		storage:     storage,
		peerIDs:     peerIDs,
		commitChan:  commitChan,
		peerClients: make(map[int]*rpc.Client),
		ready:       ready,
		quit:        make(chan struct{}),
		logger:      logger,
	}

	return s
}

// Serve will start the rpc server and accept requests.
func (s *Server) Serve() {
	s.mu.Lock()
	s.cm = NewConsensusModule(s.id, s.peerIDs, s, s.storage, s.logger, s.ready, s.commitChan)

	// Create a new RPC server and register a RPCProxy that forwards all methods to n.cm
	s.rpcServer = rpc.NewServer()
	s.rpcProxy = &RPCProxy{cm: s.cm}
	err := s.rpcServer.RegisterName("ConsensusModule", s.rpcProxy)
	if err != nil {
		s.cm.logger.Error(err, "failed to register rpc server")
		panic(err)
	}

	s.listener, err = net.Listen("tcp", ":0")
	if err != nil {
		s.cm.logger.Error(err, "failed to create listener")
		panic(err)
	}
	s.cm.logger.Info("listening", "addr", s.listener.Addr())
	s.mu.Unlock()

	s.wg.Add(1)
	go func() {
		defer s.wg.Done()

		for {
			con, err := s.listener.Accept()
			if err != nil {
				select {
				case <-s.quit:
					return
				default:
					s.cm.logger.Error(err, "accept error")
					panic(err)
				}
			}

			s.wg.Add(1)
			go func() {
				s.rpcServer.ServeConn(con)
				s.wg.Done()
			}()
		}
	}()
}

// DisconnectAll closes all the client connection to peers for this server.
func (s *Server) DisconnectAll() {
	s.mu.Lock()
	defer s.mu.Unlock()
	for id := range s.peerClients {
		if s.peerClients[id] != nil {
			s.peerClients[id].Close()
			s.peerClients[id] = nil
		}
	}
}

// Shutdown closes the server and waits for it to shut down properly.
func (s *Server) Shutdown() {
	s.cm.Stop()
	close(s.quit)
	s.listener.Close()
	s.wg.Wait()
}

// ListenAddr returns the listen address of the server
func (s *Server) ListenAddr() net.Addr {
	s.mu.Lock()
	defer s.mu.Unlock()
	return s.listener.Addr()
}

// ConnectToPeer connects to the RPC server of the specified peer.
func (s *Server) ConnectToPeer(id int, addr net.Addr) error {
	s.mu.Lock()
	defer s.mu.Unlock()
	if s.peerClients[id] != nil {
		return nil
	}

	client, err := rpc.Dial(addr.Network(), addr.String())
	if err != nil {
		return err
	}
	s.peerClients[id] = client

	return nil
}

// DisconnectPeer closes the RPC connection with the peer and nullifies the
// connection.
func (s *Server) DisconnectPeer(id int) error {
	s.mu.Lock()
	defer s.mu.Unlock()
	if s.peerClients[id] == nil {
		return nil
	}
	err := s.peerClients[id].Close()
	s.peerClients[id] = nil

	return err
}

// Call sends the RPC request with the given arguments and marshals the request to the given reply.
func (s *Server) Call(id int, serviceMethod string, args interface{}, reply interface{}) error {
	s.mu.Lock()
	peer := s.peerClients[id]
	s.mu.Unlock()

	if peer == nil {
		return fmt.Errorf("call client %d after it's closed", id)
	}

	return peer.Call(serviceMethod, args, reply)
}

// RPCProxy is a trivial pass-thru proxy type for ConsensusModule's RPC methods.
// It's useful for:
// - Simulating a small delay in RPC transmission.
// - Avoid running into https://github.com/golang/go/issues/19957
// - Simulating possible unreliable connection by delaying some messages
// significantly and dropping others when RAFT_UNRELIABLE_RPC is set.
type RPCProxy struct {
	cm *ConsensusModule
}

// RequestVote sends the RequestVote RPC to the CM introducing some delays.
func (rpp *RPCProxy) RequestVote(args RequestVoteArgs, reply *RequestVoteReply) error {
	logger := rpp.cm.logger.WithValues("rpc", "RequestVote")

	sleepDuration, err := rpp.raftUnreliableRPC()
	if err != nil {
		logger.Error(err, "drop")
	}

	logger.Info("delay", "delay_ms", sleepDuration.Milliseconds())

	time.Sleep(sleepDuration)

	return rpp.cm.RequestVote(args, reply)
}

// AppendEntries sends the RequestVote RPC to the CM introducing some delays.
func (rpp *RPCProxy) AppendEntries(args AppendEntriesArgs, reply *AppendEntriesReply) error {
	logger := rpp.cm.logger.WithValues("rpc", "AppendEntries")

	sleepDuration, err := rpp.raftUnreliableRPC()
	if err != nil {
		logger.Error(err, "drop")
	}

	logger.Info("delay", "delay_ms", sleepDuration.Milliseconds())

	time.Sleep(sleepDuration)
	return rpp.cm.AppendEntries(args, reply)
}

func (rpp *RPCProxy) raftUnreliableRPC() (time.Duration, error) {
	sleepDuration := time.Duration(1+rand.Intn(5)) * time.Millisecond

	if os.Getenv("RAFT_UNRELIABLE_RPC") != "" {
		switch rand.Intn(10) {
		case 9:
			return sleepDuration, fmt.Errorf("RPC Failed")
		case 8:
			sleepDuration = 75 * time.Millisecond
		}
	}

	return sleepDuration, nil
}
