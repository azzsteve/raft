package raft

import (
	"fmt"
	"sync"
	"testing"
	"time"

	"github.com/go-logr/logr"
	"github.com/go-logr/zapr"
	"go.uber.org/zap"
)

type Harness struct {
	mu sync.Mutex

	// cluster is a list of all the raft servers participating in a cluster.
	cluster []*Server
	storage []*MapStorage

	// commitChan has a channel per server in cluster with the commit channel
	// for that server.
	commitChan []chan CommitEntry

	// commits at index i holds the sequence of commits made by server i so far.
	// It is populated by goroutines that listen on the corresponding
	// commitChan channel.
	commits [][]CommitEntry

	// connected has a bool per server in cluster, specifying whether this
	// server is currently connected to peers. If it's set to false it's
	// partitioned and no messages will pass to and from it.
	connected []bool

	// alive has a bool per server in cluster, specifying weather this server is
	// currently alive. `false` means it crash and wasn't restarted yet.
	// Connected implies alive
	alive []bool

	n int
	t *testing.T

	logger logr.Logger
}

// NewHarness creates a new test Harness, initialized with n servers connected
// to each other.
func newHarness(t *testing.T, n int) *Harness {
	ns := make([]*Server, n)
	connected := make([]bool, n)
	commitChan := make([]chan CommitEntry, n)
	ready := make(chan struct{})
	storage := make([]*MapStorage, n)
	alive := make([]bool, n)

	var logger logr.Logger

	zapLog, err := zap.NewDevelopment()
	if err != nil {
		panic(fmt.Sprintf("who watches the watchmen (%v)?", err))
	}
	logger = zapr.NewLogger(zapLog)

	// Create all Servers in this cluster, assign ids and peer ids.
	for i := 0; i < n; i++ {
		peerIDs := make([]int, 0)
		for p := 0; p < n; p++ {
			// If the peer has the same ID as the server, skip it.
			if p == i {
				continue
			}

			peerIDs = append(peerIDs, p)
		}

		commitChan[i] = make(chan CommitEntry)
		storage[i] = NewMapStorage()
		ns[i] = NewServer(i, peerIDs, storage[i], logger, ready, commitChan[i])
		ns[i].Serve()
		alive[i] = true
	}

	// Connect all peers to each other.
	for i := 0; i < n; i++ {
		for j := 0; j < n; j++ {
			err := ns[i].ConnectToPeer(j, ns[j].ListenAddr())
			if err != nil {
				t.Logf("Failed to connect to peer %v", err)
				t.FailNow()
			}
		}
		connected[i] = true
	}
	close(ready)

	h := &Harness{
		cluster:    ns,
		storage:    storage,
		connected:  connected,
		alive:      alive,
		commitChan: commitChan,
		commits:    make([][]CommitEntry, n),
		n:          n,
		t:          t,
		logger:     logger.WithName("test"),
	}

	for i := 0; i < n; i++ {
		go h.collectCommits(i)
	}

	return h
}

// Shutdown first disconnects all the connections with the peers and then
// shutdown the server of each node in the cluster.
func (h *Harness) Shutdown() {
	for i := 0; i < h.n; i++ {
		h.cluster[i].DisconnectAll()
		h.connected[i] = false
	}

	for i := 0; i < h.n; i++ {
		if h.alive[i] {
			h.alive[i] = false
			h.cluster[i].Shutdown()
		}
	}

	for i := 0; i < h.n; i++ {
		close(h.commitChan[i])
	}
}

// DisconnectPeer disconnects a server from all the other servers in a cluster.
func (h *Harness) DisconnectPeer(id int) {
	h.logger.Info("disconnect", "peer_id", id)
	// Disconnect all the peers.
	h.cluster[id].DisconnectAll()

	// Remove the connection of this server from all the other nodes in the
	// cluster.
	for j := 0; j < h.n; j++ {
		if j != id {
			h.cluster[j].DisconnectPeer(id)
		}
	}
	h.connected[id] = false
}

// CrashPeer "crashes" a server by disconnecting it from all peers and then
// asking it to shutdown. we're not going to use the same server instance again,
// but its storage is retained
func (h *Harness) CrashPeer(id int) {
	h.t.Logf("crash %d", id)
	h.DisconnectPeer(id)
	h.alive[id] = false
	h.cluster[id].Shutdown()

	// Clear out the commits slice for the crashed server; Raft assumes the
	// client has no persistent state. Once this server comes back online it
	// will replay the whole log to us.
	h.mu.Lock()
	h.commits[id] = h.commits[id][:0]
	h.mu.Unlock()
}

// ReconnectPeer connects a server to all other services in the cluster.
func (h *Harness) ReconnectPeer(id int) {
	h.logger.Info("reconnect", "peer_id", id)
	for j := 0; j < h.n; j++ {
		if j == id || !h.alive[j] {
			continue
		}

		// Connect node to the rest of the cluster.
		err := h.cluster[id].ConnectToPeer(j, h.cluster[j].ListenAddr())
		if err != nil {
			h.logger.Error(err, "failed to connect node to peers")
		}

		// Connect the cluster nodes to this node.
		err = h.cluster[j].ConnectToPeer(id, h.cluster[id].ListenAddr())
		if err != nil {
			h.logger.Error(err, "failed to connect peers to node")
		}
	}

	h.connected[id] = true
}

// RestartPeer "restarts" a server by creating a new Server instance and giving it
// the appropriate storage, reconnecting it to peers.
func (h *Harness) RestartPeer(id int) {
	if h.alive[id] {
		h.t.Fatalf("%d is already alive", id)
	}
	h.t.Logf("Restart %d", id)

	peerIDs := make([]int, 0)
	for p := 0; p < h.n; p++ {
		if p == id {
			continue
		}

		peerIDs = append(peerIDs, p)
	}

	ready := make(chan struct{})
	h.cluster[id] = NewServer(id, peerIDs, h.storage[id], h.logger, ready, h.commitChan[id])
	h.cluster[id].Serve()
	h.ReconnectPeer(id)
	close(ready)
	h.alive[id] = true
	time.Sleep(100 * time.Millisecond)
}

// CheckSingleLeader checks that only a single server thinks it's a leader.
// Returns the leader's ID and term. It retries several time if no leader is
// identified yet.
func (h *Harness) CheckSingleLeader() (int, int) {
	for a := 0; a < 5; a++ {
		leaderID := -1
		leaderTerm := -1
		for i := 0; i < h.n; i++ {
			if !h.connected[i] {
				time.Sleep(150 * time.Millisecond)
				continue
			}
			_, term, leader := h.cluster[i].cm.Report()
			if leader {
				if leaderID > 0 {
					h.logger.Info("have two nodes thinking they are leaders", "node_1", leaderID, "node_2", i)
					h.t.Fatal("more then 1 leader")
				}
				leaderID = i
				leaderTerm = term
			}
		}

		if leaderID >= 0 {
			return leaderID, leaderTerm
		}
		time.Sleep(150 * time.Millisecond)
	}

	h.t.Fatal("leader not found")

	return -1, -1
}

// CheckNoLeader checks that no connected server considers itself the leader.
func (h *Harness) CheckNoLeader() {
	for i := 0; i < h.n; i++ {
		if !h.connected[i] {
			continue
		}
		_, _, leader := h.cluster[i].cm.Report()
		if leader {
			h.t.Fatal("found leader")
		}
	}
}

// SubmitToServer submits the command to srvID..
func (h *Harness) SubmitToServer(srvID int, cmd interface{}) bool {
	return h.cluster[srvID].cm.Submit(cmd)
}

// CheckCommitted verifies that all connected servers have cmd committed with
// the same index. It also verifies that all commands *before* cmd in the commit
// sequence match. For this to work properly, all commands submitted to Raft
// should be unique positive ints.
//
// Return the number of servers that have this command committed, and it's log
// index.
func (h *Harness) CheckCommitted(cmd interface{}) (int, int) {
	h.mu.Lock()
	defer h.mu.Unlock()

	// Find the length of the commits slice for connected servers.
	commitsLen := -1
	for i := 0; i < h.n; i++ {
		if !h.connected[i] {
			continue
		}

		if commitsLen >= 0 {
			// Fail if the commits length don't match with the previous server
			// that we checked.
			if len(h.commits[i]) != commitsLen {
				h.t.Fatalf("commits[%d] = %d, commitsLen = %d", i, h.commits[i], commitsLen)
			}
		} else {
			commitsLen = len(h.commits[i])
		}
	}

	// Check consistency of commits from the start and to the command we're asked
	// about. This loop will return once a command=cmd is found.
	for c := 0; c < commitsLen; c++ {
		cmdAtC := -1
		for i := 0; i < h.n; i++ {
			if !h.connected[i] {
				continue
			}

			cmdOfN, ok := h.commits[i][c].Command.(int)
			if !ok {
				h.t.Fatalf("failed to type assert to int")
			}

			if cmdAtC >= 0 {
				// Check that the command matches with the other servers.
				if cmdOfN != cmdAtC {
					h.t.Errorf("got %d, want %d at h.commits[%d][%d]", cmdOfN, cmdAtC, i, c)
				}
			} else {
				cmdAtC = cmdOfN
			}
		}

		if cmdAtC == cmd {
			// Check consistency of Index.
			index := -1
			nc := 0
			for i := 0; i < h.n; i++ {
				if !h.connected[i] {
					continue
				}

				if index >= 0 && h.commits[i][c].Index != index {
					h.t.Errorf("got Index=%d, want %d at h.commits[%d][%d]", h.commits[i][c].Index, index, i, c)
				} else {
					index = h.commits[i][c].Index
				}
				nc++
			}
			return nc, index
		}
	}

	// If there's not early return, we haven't found the command we are looking
	// for.
	h.t.Errorf("cmd=%d not found in commits", cmd)
	return -1, -1
}

// CheckCommittedN verifies thai cmd was committed by exactly n connected server.
func (h *Harness) CheckCommittedN(cmd int, n int) {
	nc, _ := h.CheckCommitted(cmd)
	if nc != n {
		h.t.Errorf("total number %d, want %d", nc, n)
	}
}

func (h *Harness) CheckNotCommitted(cmd int) {
	h.mu.Lock()
	defer h.mu.Unlock()

	for i := 0; i < h.n; i++ {
		if !h.connected[i] {
			continue
		}

		for c := 0; c < len(h.commits[i]); c++ {
			gotCmd, ok := h.commits[i][c].Command.(int)
			if !ok {
				h.t.Fatalf("failed to type assert to int %v", h.commits[i][c].Command)
			}

			if gotCmd == cmd {
				h.t.Errorf("found %d at commits[%d][%d]", cmd, i, c)
			}
		}
	}
}

// collectCommits reads channel commitChan[i] and adds all received entries to
// the corresponding commits [i] It's blocking and should be run in a separate
// goroutine. It returns when commitChan[i] is closed.
func (h *Harness) collectCommits(i int) {
	for c := range h.commitChan[i] {
		h.mu.Lock()
		h.t.Logf("collectCommits[%d] got %+v", i, c)
		h.commits[i] = append(h.commits[i], c)
		h.mu.Unlock()
	}
}
