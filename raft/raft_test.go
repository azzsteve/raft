package raft

import (
	"fmt"
	"testing"
	"time"

	"github.com/fortytw2/leaktest"
)

func TestElectionBasic(t *testing.T) {
	h := newHarness(t, 3)
	defer h.Shutdown()

	h.CheckSingleLeader()
}

func TestElectionLeaderDisconnect(t *testing.T) {
	h := newHarness(t, 3)
	defer h.Shutdown()

	oldLeaderID, oldTerm := h.CheckSingleLeader()

	h.DisconnectPeer(oldLeaderID)
	time.Sleep(350 * time.Millisecond)

	newLeaderID, newTerm := h.CheckSingleLeader()
	if newLeaderID == oldLeaderID {
		t.Errorf("want new leader to be different from the original leader")
	}

	if newTerm <= oldTerm {
		t.Errorf("want newTerm >= oldTerm, newTerm: %d oldTerm: %d", newTerm, oldTerm)
	}
}

func TestElectionLeaderAndAnotherDisconnect(t *testing.T) {
	h := newHarness(t, 3)
	defer h.Shutdown()

	oldLeaderID, _ := h.CheckSingleLeader()

	h.DisconnectPeer(oldLeaderID)
	otherID := (oldLeaderID + 1) % 3
	h.DisconnectPeer(otherID)

	// No quorum
	time.Sleep(450 * time.Millisecond)
	h.CheckNoLeader()

	// Reconnect one other server; now we'll have quorum.
	h.ReconnectPeer(otherID)

	h.CheckSingleLeader()
}

func TestDisconnectAllThenRestore(t *testing.T) {
	h := newHarness(t, 3)
	defer h.Shutdown()

	time.Sleep(100 * time.Millisecond)

	// Disconnect all servers from the start. there wil be no leader.
	for i := 0; i < 3; i++ {
		h.DisconnectPeer(i)
	}

	time.Sleep(450 * time.Millisecond)
	h.CheckNoLeader()

	// Reconnect all servers. A leader will be found.
	for i := 0; i < 3; i++ {
		h.ReconnectPeer(i)
	}

	h.CheckSingleLeader()
}

func TestElectionLeaderDisconnectThenReconnect(t *testing.T) {
	tests := []struct {
		numberOfNodes int
	}{
		{
			numberOfNodes: 3,
		},
		{
			numberOfNodes: 5,
		},
	}

	for _, tt := range tests {
		t.Run(fmt.Sprintf("total %d", tt.numberOfNodes), func(t *testing.T) {
			defer leaktest.CheckTimeout(t, 100*time.Millisecond)

			h := newHarness(t, tt.numberOfNodes)
			defer h.Shutdown()

			oldLeaderID, _ := h.CheckSingleLeader()

			h.DisconnectPeer(oldLeaderID)

			time.Sleep(350 * time.Millisecond)
			newLeaderID, newTerm := h.CheckSingleLeader()

			h.ReconnectPeer(oldLeaderID)
			time.Sleep(150 * time.Millisecond)

			againLeaderID, againTerm := h.CheckSingleLeader()

			if againLeaderID != newLeaderID {
				println(oldLeaderID)
				t.Errorf("wanted the leader to not change got %d; want %d", againLeaderID, newLeaderID)
			}

			if againTerm != newTerm {
				t.Errorf("wanted term to not change got %d; want %d", againTerm, newTerm)
			}
		})
	}
}

func TestElectionFollowerComesBack(t *testing.T) {
	defer leaktest.CheckTimeout(t, 100*time.Millisecond)

	h := newHarness(t, 3)
	defer h.Shutdown()

	oldLeaderID, oldTerm := h.CheckSingleLeader()

	otherID := (oldLeaderID + 1) % 3
	h.DisconnectPeer(otherID)
	time.Sleep(650 * time.Millisecond)
	h.ReconnectPeer(otherID)
	time.Sleep(150 * time.Millisecond)

	// We can't have an assertion on the new id here because it depends on the
	// relative election timeouts. We can assert that the term changed, however,
	// which implies that re-election has occurred.
	_, newTerm := h.CheckSingleLeader()
	if newTerm <= oldTerm {
		t.Errorf("term is the same or lower newTerm: %d oldTerm %d", newTerm, oldTerm)
	}
}

func TestElectionDisconnectLoop(t *testing.T) {
	defer leaktest.CheckTimeout(t, 100*time.Millisecond)

	h := newHarness(t, 3)
	defer h.Shutdown()

	for cycle := 0; cycle < 5; cycle++ {
		leaderID, _ := h.CheckSingleLeader()

		h.DisconnectPeer(leaderID)
		otherID := (leaderID + 1) % 3
		h.DisconnectPeer(otherID)

		time.Sleep(350 * time.Millisecond)
		h.CheckNoLeader()

		// Reconnect both.
		h.ReconnectPeer(otherID)
		h.ReconnectPeer(leaderID)

		time.Sleep(150 * time.Millisecond)
	}
}

func TestCommitOneCommand(t *testing.T) {
	defer leaktest.CheckTimeout(t, 100*time.Millisecond)

	h := newHarness(t, 3)
	defer h.Shutdown()

	leaderID, _ := h.CheckSingleLeader()

	t.Logf("submitting 42 to %d", leaderID)
	isLeader := h.SubmitToServer(leaderID, 42)
	if !isLeader {
		t.Errorf("want id=%d leader, but it's not", leaderID)
	}

	time.Sleep(150 * time.Millisecond)
	h.CheckCommittedN(42, 3)
}

func TestSubmitNonLeaderFails(t *testing.T) {
	h := newHarness(t, 3)
	defer h.Shutdown()

	leaderID, _ := h.CheckSingleLeader()
	sid := (leaderID + 1) % 3

	t.Logf("submitting 42 to %d", sid)
	isLeader := h.SubmitToServer(sid, 42)
	if isLeader {
		t.Errorf("got lead=%d, should be %d", sid, leaderID)
	}

	time.Sleep(10 * time.Millisecond)
}

func TestCommitMultipleCommands(t *testing.T) {
	defer leaktest.CheckTimeout(t, 100*time.Millisecond)

	h := newHarness(t, 3)
	defer h.Shutdown()

	leaderID, _ := h.CheckSingleLeader()

	values := []int{42, 55, 81}
	for _, v := range values {
		t.Logf("submitting %d to %d", v, leaderID)
		isLeader := h.SubmitToServer(leaderID, v)
		if !isLeader {
			t.Errorf("%d is not a leader", leaderID)
		}
		time.Sleep(100 * time.Millisecond)
	}

	time.Sleep(150 * time.Millisecond)
	nc, i1 := h.CheckCommitted(values[0])
	_, i2 := h.CheckCommitted(values[1])
	if nc != 3 {
		t.Errorf("got %d want nc=3", nc)
	}

	// Check index
	if i1 >= i2 {
		t.Errorf("got i1=%d i2=%d want i1<i2", i1, i2)
	}

	_, i3 := h.CheckCommitted(values[2])
	if i2 >= i3 {
		t.Errorf("got i2=%d i3=%d want i2<i3", i2, i3)
	}
}

func TestCommitWithDisconnectionAndRecover(t *testing.T) {
	defer leaktest.CheckTimeout(t, 100*time.Millisecond)

	h := newHarness(t, 3)
	defer h.Shutdown()

	// Submit a couple of values to a fully connected cluster
	leaderID, _ := h.CheckSingleLeader()
	h.SubmitToServer(leaderID, 5)
	h.SubmitToServer(leaderID, 6)

	time.Sleep(500 * time.Millisecond)
	h.CheckCommittedN(6, 3)

	dPeerID := (leaderID + 1) % 3
	h.DisconnectPeer(dPeerID)
	time.Sleep(500 * time.Millisecond)

	// Submit a new command, it will be committed but only to two servers.
	h.SubmitToServer(leaderID, 7)
	time.Sleep(500 * time.Millisecond)
	h.CheckCommittedN(7, 2)

	// Now reconnect dPeerID and wait a bit; it should find the new command too.
	h.ReconnectPeer(dPeerID)
	time.Sleep(500 * time.Millisecond)
	h.CheckSingleLeader()

	h.CheckCommittedN(7, 3)
}

func TestNoCommitWithQuorum(t *testing.T) {
	defer leaktest.CheckTimeout(t, 100*time.Millisecond)

	h := newHarness(t, 3)
	defer h.Shutdown()

	// Submit a couple of values to a fully connected cluster
	leaderID, term := h.CheckSingleLeader()
	h.SubmitToServer(leaderID, 5)
	h.SubmitToServer(leaderID, 6)

	time.Sleep(500 * time.Millisecond)
	h.CheckCommittedN(6, 3)

	// Disconnect both followers
	peer1 := (leaderID + 1) % 3
	peer2 := (leaderID + 2) % 3
	h.DisconnectPeer(peer1)
	h.DisconnectPeer(peer2)
	time.Sleep(250 * time.Millisecond)

	h.SubmitToServer(leaderID, 8)
	time.Sleep(500 * time.Millisecond)
	h.CheckNotCommitted(8)

	// Reconnect both other servers, we'll have quorum now.
	h.ReconnectPeer(peer1)
	h.ReconnectPeer(peer2)
	time.Sleep(800 * time.Millisecond)

	// 8 is still not committed because the term has changed.
	h.CheckNotCommitted(8)

	// A New leader will be elected. It could be a different leader, even tough
	// the original's log is longer, because the two reconnected peers can elect
	// each other.
	newLeaderID, newTerm := h.CheckSingleLeader()
	if term == newTerm {
		t.Errorf("got %d but should be a different term", newTerm)
	}

	// New values will be committed
	h.SubmitToServer(newLeaderID, 9)
	h.SubmitToServer(newLeaderID, 10)
	h.SubmitToServer(newLeaderID, 11)
	time.Sleep(500 * time.Millisecond)

	for _, v := range []int{9, 10, 11} {
		h.CheckCommittedN(v, 3)
	}
}

func TestCommitsWithLeaderDisconnects(t *testing.T) {
	defer leaktest.CheckTimeout(t, 100*time.Millisecond)

	h := newHarness(t, 5)
	defer h.Shutdown()

	// Submit a couple of values to a fully connected cluster.
	leaderID, _ := h.CheckSingleLeader()
	h.SubmitToServer(leaderID, 5)
	h.SubmitToServer(leaderID, 6)

	time.Sleep(550 * time.Millisecond)
	h.CheckCommittedN(6, 5)

	// Leader disconnect
	h.DisconnectPeer(leaderID)
	time.Sleep(10 * time.Millisecond)

	// Submit 7 to original leader, even though it's disconnected
	h.SubmitToServer(leaderID, 7)

	time.Sleep(550 * time.Millisecond)
	h.CheckNotCommitted(7)

	newLeaderID, _ := h.CheckSingleLeader()

	// Submit 8 to new leader
	h.SubmitToServer(newLeaderID, 8)
	time.Sleep(550 * time.Millisecond)
	h.CheckCommittedN(8, 4)

	// Reconnect old leader and let is settle. The old leader shouldn't be the
	// one winning.
	h.ReconnectPeer(leaderID)
	time.Sleep(800 * time.Millisecond)

	finalLeaderID, _ := h.CheckSingleLeader()
	if finalLeaderID == leaderID {
		t.Errorf("finalLeadID == leaderID %d", leaderID)
	}

	// Submit 9 and check it's fully committed.
	h.SubmitToServer(finalLeaderID, 9)
	time.Sleep(550 * time.Millisecond)
	h.CheckCommittedN(9, 5)
	h.CheckCommittedN(8, 5)

	// but 7 is not comitted.
	h.CheckNotCommitted(7)
}

func TestCrashFollower(t *testing.T) {
	defer leaktest.CheckTimeout(t, 100*time.Millisecond)

	h := newHarness(t, 3)
	defer h.Shutdown()

	leaderID, _ := h.CheckSingleLeader()
	h.SubmitToServer(leaderID, 5)

	time.Sleep(500 * time.Millisecond)
	h.CheckCommittedN(5, 3)

	h.CrashPeer((leaderID + 1) % 3)
	time.Sleep(500 * time.Millisecond)
	h.CheckCommittedN(5, 2)
}

func TestCrashThenRestartFollower(t *testing.T) {
	defer leaktest.CheckTimeout(t, 100*time.Millisecond)

	commits := []int{5, 6, 7}

	h := newHarness(t, 3)
	defer h.Shutdown()

	leaderID, _ := h.CheckSingleLeader()

	for _, c := range commits {
		h.SubmitToServer(leaderID, c)
	}

	time.Sleep(500 * time.Millisecond)
	for _, c := range commits {
		h.CheckCommittedN(c, 3)
	}

	h.CrashPeer((leaderID + 1) % 3)
	time.Sleep(350 * time.Millisecond)

	for _, c := range commits {
		h.CheckCommittedN(c, 2)
	}

	// Restart the crashed follower and give it some time to come up-to-date.
	h.RestartPeer((leaderID + 1) % 3)
	time.Sleep(time.Second)

	for _, c := range commits {
		h.CheckCommittedN(c, 3)
	}
}

func TestCrashThenRestartLeader(t *testing.T) {
	defer leaktest.CheckTimeout(t, 100*time.Millisecond)

	commits := []int{5, 6, 7}

	h := newHarness(t, 3)
	defer h.Shutdown()

	leaderID, _ := h.CheckSingleLeader()

	for _, c := range commits {
		h.SubmitToServer(leaderID, c)
	}

	time.Sleep(500 * time.Millisecond)
	for _, c := range commits {
		h.CheckCommittedN(c, 3)
	}

	h.CrashPeer(leaderID)

	time.Sleep(500 * time.Millisecond)
	for _, c := range commits {
		h.CheckCommittedN(c, 2)
	}

	h.RestartPeer(leaderID)

	time.Sleep(time.Second)
	for _, c := range commits {
		h.CheckCommittedN(c, 3)
	}
}

func TestCrashThenRestartAll(t *testing.T) {
	defer leaktest.CheckTimeout(t, 100*time.Millisecond)

	commits := []int{5, 6, 7}

	h := newHarness(t, 3)
	defer h.Shutdown()

	leaderID, _ := h.CheckSingleLeader()

	for _, c := range commits {
		h.SubmitToServer(leaderID, c)
	}

	time.Sleep(500 * time.Millisecond)
	for _, c := range commits {
		h.CheckCommittedN(c, 3)
	}

	// Crash all servers
	for i := 0; i < 3; i++ {
		h.CrashPeer((leaderID + i) % 3)
	}

	time.Sleep(500 * time.Millisecond)

	// Restart all servers
	for i := 0; i < 3; i++ {
		h.RestartPeer((leaderID + i) % 3)
	}

	time.Sleep(500 * time.Millisecond)

	newLeaderID, _ := h.CheckSingleLeader()
	commits = append(commits, 8)

	h.SubmitToServer(newLeaderID, commits[len(commits)-1:][0])

	time.Sleep(500 * time.Millisecond)
	for _, c := range commits {
		h.CheckCommittedN(c, 3)
	}
}
